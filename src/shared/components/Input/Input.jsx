import React from "react";
import './Input.scss'

const Input = ({type, value, name, onClick, className}) => {
    return (

        <input className={className} type={type} value={value} name={name} onClick={onClick}/>

        )
};

export default Input;
