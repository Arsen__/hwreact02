export const OPEN_MODAL = "OPEN_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ITEM_ADD_ORANGE = 'ITEM_ADD_ORANGE';
export const ITEM_ADD_BANANA = 'ITEM_ADD_BANANA';
export const ITEM_ADD_LEMON = 'ITEM_ADD_LEMON';
export const CLEAR_CART = 'CLEAR_CART';
export const ITEM_REMOVE = 'ITEM_REMOVE';
