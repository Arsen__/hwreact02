import {ITEM_ADD_ORANGE, ITEM_ADD_LEMON, ITEM_ADD_BANANA, ITEM_REMOVE, CLEAR_CART} from "../constants/contstants";

const initialState =  [];

export const itemReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case ITEM_ADD_ORANGE:
            const orange = payload.count < 2 ? [...state, {productName: payload.name, productPrice: payload.price, productCount: payload.count}] : [{productName: payload.name, productPrice: payload.price, productCount: payload.count}]
            console.log(orange)
            return orange

        case ITEM_ADD_BANANA:
            const banana = payload.count < 2 ? [...state, {productName: payload.name, productPrice: payload.price, productCount: payload.count}] : [{productName: payload.name, productPrice: payload.price, productCount: payload.count}]
            return banana

        case ITEM_ADD_LEMON:
            const lemon = payload.count < 2 ? [...state, {productName: payload.name, productPrice: payload.price, productCount: payload.count}] : [{productName: payload.name, productPrice: payload.price, productCount: payload.count}]
            return lemon
        case ITEM_REMOVE:
            const newState = [...state];
            newState.splice(payload, 1);
            return newState;
        case CLEAR_CART:
            return [];
        default:
            return state;
    }
}
