import { combineReducers } from 'redux'
import {modalReducer} from './modalReducer';
import {itemReducer} from './itemReducer';


export default combineReducers({
    modalOpen: modalReducer,
    items: itemReducer,
});
