import {OPEN_MODAL, CLOSE_MODAL} from "../constants/contstants";

const initialState =  {
    openModal: false,
};

export const modalReducer = (state = initialState, {type}) => {
    switch(type) {
        case OPEN_MODAL:
            return {...state, openModal: true};
        case CLOSE_MODAL:
            return {...state, openModal: false};
        default:
            return state;
    }
}
