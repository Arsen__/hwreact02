import {ITEM_REMOVE} from "../constants/contstants";

export const actionItemRemove = (payload) => {
    return {
        type: ITEM_REMOVE,
        payload,
    }
};
