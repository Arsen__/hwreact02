import {CLOSE_MODAL} from "../constants/contstants";

export const actionModalClose = ()=> {
    return {
        type: CLOSE_MODAL
    }
};
