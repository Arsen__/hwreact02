import {CLEAR_CART} from "../constants/contstants";

export const actionClearCart = () => {
    return {
        type: CLEAR_CART,
    }
};
