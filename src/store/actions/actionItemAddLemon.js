import {ITEM_ADD_LEMON} from "../constants/contstants";

export const actionItemAddLemon = (payload) => {
    return {
        type: ITEM_ADD_LEMON,
        payload
    }
};
