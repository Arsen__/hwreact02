import {ITEM_ADD_BANANA} from "../constants/contstants";

export const actionItemAddBanana = (payload) => {
    return {
        type: ITEM_ADD_BANANA,
        payload
    }
};
