import {OPEN_MODAL} from "../constants/contstants";

export const actionModalOpen = () => {
    return {
        type: OPEN_MODAL
    }
};
