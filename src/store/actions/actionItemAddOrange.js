import {ITEM_ADD_ORANGE} from "../constants/contstants";

export const actionItemAddOrange = (payload) => {
    return {
        type: ITEM_ADD_ORANGE,
        payload
    }
};
