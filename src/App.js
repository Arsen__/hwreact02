import React from 'react';
import './App.css';
import StoreList from "./client/components/StoreList";
import {Provider} from 'react-redux';
import {store} from "./store/store";
import Modal from "./client/components/Modal";

function App() {
  return (
      <Provider store={store}>
          <Modal />
          <StoreList />
      </Provider>
  );
}

export default App;
