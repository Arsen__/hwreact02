import React from "react";
import {useDispatch, useSelector} from 'react-redux'
import './Modal.scss'
import Input from "../../../shared/components/Input";
import {actionModalClose} from "../../../store/actions";
import ModalItem from "./ModalItem/ModalItem";

const Modal = () => {
    const dispatch = useDispatch();
    const {openModal} = useSelector((state) => state.modalOpen);
    const className = openModal ? 'modal active' : 'modal';
    const items = useSelector((state) => state.items);
    const modalItem = items.map(item =>  <ModalItem {...item}/> )
    const handleClose = () => {
        const action = actionModalClose();
        dispatch(action);
    }



    return(
        <div className={className}>
            <h3 className={'modal-header'}>Cart</h3>
            <span className={'modal-cross'} onClick={() => handleClose()}>X</span>
            <hr className={'modal-hr'}/>
            {modalItem}
            <p className={'modal-text'}>Total price: </p>
            <hr className={'modal-hr'}/>
            <div className={'modal-buttons'}>
                <Input className={'modal-close'} type={'button'} onClick={() => handleClose()} name={'modal-close'} value={'Close'}/>
            </div>
        </div>
    )
}

export default Modal;