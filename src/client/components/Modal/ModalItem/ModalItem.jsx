import React from "react";
import './ModalItem.scss'
import Input from "../../../../shared/components/Input";

const ModalItem = ({productName, productPrice}) => {
    const showItem = productPrice > 0 ? 'show-item.true' : 'show-item'
    return (
        <div className={showItem}>
            <ul className={`modal-item`}>
                <li className={'modal-item-text'}>{productName}</li>
                <li className={'modal-item-text'}>{productPrice}</li>
                <li className={'modal-item-li modal-item-list'}>
                    <Input type={'button'} className={'modal-minus'} value={'-'}/>
                    <Input type={'number'} className={'modal-item-number-input'} />
                    <Input type={'button'} className={'modal-plus'} value={'+'}/>
                    <Input className={'modal-item-red-btn'} value={'X'} type={'button'}/>
                </li>
                <li className={'modal-item-text'}>{productPrice}</li>
            </ul>
        </div>
    )
};

export default ModalItem;