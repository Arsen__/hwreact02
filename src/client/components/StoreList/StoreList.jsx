import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import './StoreList.scss';
import {fields} from './fields';
import StoreListButtons from "./StoreListButtons";
import StoreListItem from "./StoreListItem";
import {actionClearCart, actionModalOpen} from '../../../store/actions';

const StoreList = () => {
    const dispatch = useDispatch();
    const item = useSelector((state) => state.items);
    const handleClick = () => {
        const action = actionModalOpen();
        dispatch(action);
    };
    const handleClear = () => {
        const action = actionClearCart();
        dispatch(action)
    };

    return (
        <>
            <StoreListButtons cartOnClick={() => handleClick()} clearOnClick={() => handleClear()} count={0}/>
            <div className={'store-list-container'}>
                <StoreListItem {...fields.orange}/>
                <StoreListItem {...fields.banana}/>
                <StoreListItem {...fields.lemon}/>
            </div>
        </>
    )

};

export default StoreList;

