import React from "react";
import './StoreListButtons.scss'
import Input from "../../../../shared/components/Input";

const StoreListButtons = ({count, cartOnClick, clearOnClick}) => {
    return (
        <>
            <Input type={'button'} value={`Cart (${count})`} name={'cart-btn'} className={'cart-btn'} onClick={cartOnClick}/>
            <Input type={'button'} value={`Clear cart`} name={'cart-clear'} className={'cart-clear'} onClick={clearOnClick}/>
        </>
    )
};

export default StoreListButtons;