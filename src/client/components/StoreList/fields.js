export const fields = {
    orange: {
        headerText: 'Orange',
        pText: 'Price: $0.5',
        src: require('./img/orange.jpg'),
        btnName: 'orange',
    },
    banana: {
        headerText: 'Banana',
        pText: 'Price: $1.22',
        src: require('./img/banana.jpg'),
        btnName: 'banana',
    },
    lemon: {
        headerText: 'Orange',
        pText: 'Price: $5',
        src: require('./img/lemon.jpg'),
        btnName: 'lemon',
    }
}