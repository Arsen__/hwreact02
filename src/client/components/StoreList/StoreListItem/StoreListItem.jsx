import React, {useState} from "react";
import './StoreListItem.scss';
import {initialState} from "../initialState";
import {useDispatch} from "react-redux";
import {actionItemAddOrange} from "../../../../store/actions";
import {actionItemAddBanana} from "../../../../store/actions";
import {actionItemAddLemon} from "../../../../store/actions";

const StoreListItem = ({headerText, pText, src, btnName}) => {
    const dispatch = useDispatch();
    const [caseData, setCaseData] = useState(initialState);
    const handleAdd = (e) => {
        const name = e.target.name;
        if (name === 'orange'){
            setCaseData({
                name: 'orange',
                price: caseData.price + 0.5,
                count: caseData.count + 1,
            })
            const fullData = {...caseData};
            const action = actionItemAddOrange(fullData);
            dispatch(action);
        } else if (name === 'banana'){
            setCaseData({
                name: 'banana',
                price: caseData.price + 1.22,
                count: caseData.count + 1,
            })
            const fullData = {...caseData};
            const action = actionItemAddBanana(fullData);
            dispatch(action);
        } else if (name === 'lemon') {
            setCaseData({
                name: 'lemon',
                price: caseData.price + 5,
                count: caseData.count + 1,
            })
            const fullData = {...caseData};
            const action = actionItemAddLemon(fullData);
            dispatch(action);
        }

    }
    return (
        <div className={'store-list-item'}>
            <img src={src} alt="" className={'store-list-item-img'}/>
            <h3 className={'store-list-item-text'}>{headerText}</h3>
            <p className={'store-list-item-text'}>{pText}</p>
            <input className='store-list-button' type='button' name={btnName} value='Add to cart' onClick={(e) => handleAdd(e)}/>
        </div>
    )
};

export default StoreListItem;